<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\Unit\Application;

use PHPUnit\Framework\TestCase;
use SP\Ranking\Application\GetAbsoluteRanking;
use SP\Ranking\Tests\Unit\Fixtures\InMemoryRankingService;

final class GetAbsoluteRankingTest extends TestCase
{
    public function testGetEmptyRanking(): void
    {
        $sut = new GetAbsoluteRanking(new InMemoryRankingService());

        $output = $sut->execute(new GetAbsoluteRanking\Input(1));

        self::assertSame([], $output->jsonSerialize());
    }

    public function testGetNonEmptyRanking(): void
    {
        $sut = new GetAbsoluteRanking(new InMemoryRankingService([
            '00000000-0000-0000-0000-000000000000' => 1000,
            '00000000-0000-0000-0000-000000000001' => 0,
        ]));

        $output = $sut->execute(new GetAbsoluteRanking\Input(1));

        self::assertSame(['00000000-0000-0000-0000-000000000000' => 1000], $output->jsonSerialize());
    }

    public function testWrongInput(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new GetAbsoluteRanking\Input(-1);
    }
}

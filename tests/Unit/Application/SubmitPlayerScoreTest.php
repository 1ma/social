<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\Unit\Application;

use PHPUnit\Framework\TestCase;
use SP\Ranking\Application\SubmitPlayerScore;
use SP\Ranking\Tests\Unit\Fixtures\InMemoryRankingService;
use UMA\Uuid\SequentialGenerator;
use UMA\Uuid\UuidGenerator;

final class SubmitPlayerScoreTest extends TestCase
{
    private UuidGenerator $uuid;

    protected function setUp(): void
    {
        $this->uuid = new SequentialGenerator();
    }

    public function testSetScore(): void
    {
        $sut      = new SubmitPlayerScore($fakeRanking = new InMemoryRankingService());
        $playerId = (string) $this->uuid->generate();

        $sut->execute(new SubmitPlayerScore\Input($playerId, 100, false));

        self::assertSame(['00000000-0000-0000-0000-000000000000' => 100], $fakeRanking->peek());
    }

    public function testIncreaseScore(): void
    {
        $sut      = new SubmitPlayerScore($fakeRanking = new InMemoryRankingService());
        $playerId = (string) $this->uuid->generate();

        $sut->execute(new SubmitPlayerScore\Input($playerId, 1000, true));
        $sut->execute(new SubmitPlayerScore\Input($playerId, -200, false));
        $sut->execute(new SubmitPlayerScore\Input($playerId, 400, false));

        self::assertSame(['00000000-0000-0000-0000-000000000000' => 1200], $fakeRanking->peek());
    }
}

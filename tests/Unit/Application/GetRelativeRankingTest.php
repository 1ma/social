<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\Unit\Application;

use PHPUnit\Framework\TestCase;
use SP\Ranking\Application\GetRelativeRanking;
use SP\Ranking\Tests\Unit\Fixtures\InMemoryRankingService;

final class GetRelativeRankingTest extends TestCase
{
    public function testGetEmptyRanking(): void
    {
        $sut = new GetRelativeRanking(new InMemoryRankingService());

        $output = $sut->execute(new GetRelativeRanking\Input(2, 1));

        self::assertSame([], $output->jsonSerialize());
    }

    public function testGetNonEmptyRanking(): void
    {
        $sut = new GetRelativeRanking(new InMemoryRankingService([
            '00000000-0000-0000-0000-000000000000' => 1000,
            '00000000-0000-0000-0000-000000000001' => 500,
            '00000000-0000-0000-0000-000000000002' => 0,
        ]));

        $output = $sut->execute(new GetRelativeRanking\Input(1, 1));

        self::assertSame([
            '00000000-0000-0000-0000-000000000000' => 1000,
            '00000000-0000-0000-0000-000000000001' => 500,
            '00000000-0000-0000-0000-000000000002' => 0,
        ], $output->jsonSerialize());
    }

    public function testWrongInputOne(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new GetRelativeRanking\Input(-1, 1);
    }

    public function testWrongInputTwo(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new GetRelativeRanking\Input(1, -1);
    }
}

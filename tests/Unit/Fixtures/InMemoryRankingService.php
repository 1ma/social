<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\Unit\Fixtures;

use SP\Ranking\Application\RankingService;
use UMA\Uuid\Uuid;

final class InMemoryRankingService implements RankingService
{
    /**
     * @var array<string, int>
     */
    private array $ranking;

    public function __construct(array $ranking = [])
    {
        $this->ranking = $ranking;
    }

    public function peek(): array
    {
        return $this->ranking;
    }

    public function setScore(Uuid $playerId, int $newScore): void
    {
        $this->ranking[(string) $playerId] = $newScore;

        arsort($this->ranking);
    }

    public function alterScore(Uuid $playerId, int $amount): void
    {
        $key = (string) $playerId;
        if (!\array_key_exists($key, $this->ranking)) {
            $this->ranking[$key] = 0;
        }

        $this->ranking[$key] += $amount;

        arsort($this->ranking);
    }

    public function absoluteLeaderboard(int $topN): array
    {
        return \array_slice($this->ranking, 0, max($topN, 0));
    }

    public function relativeLeaderboard(int $rank, int $range): array
    {
        return \array_slice($this->ranking, max($rank - 1 - $range, 0), 1 + 2 * $range);
    }
}

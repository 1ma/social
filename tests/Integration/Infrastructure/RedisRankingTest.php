<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\Integration\Infrastructure;

use PHPUnit\Framework\TestCase;
use SP\Ranking\Infrastructure\RedisRanking;
use UMA\Uuid\SequentialGenerator;
use UMA\Uuid\UuidGenerator;

class RedisRankingTest extends TestCase
{
    private \Redis $client;
    private UuidGenerator $uuid;

    protected function setUp(): void
    {
        $this->client = new \Redis();
        $this->client->connect($_ENV['REDIS_HOST']);
        $this->uuid = new SequentialGenerator();
    }

    protected function tearDown(): void
    {
        $this->client->flushAll();
    }

    public function testAbsoluteLeaderboard(): void
    {
        $sut = new RedisRanking($this->client, 'testing');
        $sut->setScore($player1 = $this->uuid->generate(), 1000);
        $sut->alterScore($player1, -200);

        self::assertSame([(string) $player1 => 800], $sut->absoluteLeaderboard(1));
    }

    public function testRelativeLeaderboard(): void
    {
        $sut = new RedisRanking($this->client, 'testing');
        $sut->setScore($player1 = $this->uuid->generate(), 1000);
        $sut->setScore($player2 = $this->uuid->generate(), 100);
        $sut->setScore($player3 = $this->uuid->generate(), 0);

        self::assertSame([
            (string) $player1 => 1000,
            (string) $player2 => 100,
            (string) $player3 => 0,
        ], $sut->relativeLeaderboard(2, 1));
    }
}

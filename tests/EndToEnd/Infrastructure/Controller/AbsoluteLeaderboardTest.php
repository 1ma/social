<?php

declare(strict_types=1);

namespace SP\Ranking\Tests\EndToEnd\Infrastructure\Controller;

use GuzzleHttp\Client;
use Nyholm\Psr7\Request;
use PHPUnit\Framework\TestCase;

final class AbsoluteLeaderboardTest extends TestCase
{
    private \Redis $redis;
    private Client $http;

    protected function setUp(): void
    {
        $this->redis = new \Redis();
        $this->redis->connect($_ENV['REDIS_HOST']);
        $this->http = new Client(['base_uri' => 'http://'.$_ENV['NGINX_HOST'], 'http_errors' => false]);
    }

    protected function tearDown(): void
    {
        $this->redis->flushAll();
    }

    public function testBasicRequestToScoreController(): void
    {
        $this->http->send(new Request(
            'POST',
            '/score',
            ['Content-Type' => 'application/json'],
            '{"user": "00000000-0000-0000-0000-000000000000", "total": 1000}'
        ));

        $this->http->send(new Request(
            'POST',
            '/score',
            ['Content-Type' => 'application/json'],
            '{"user": "00000000-0000-0000-0000-000000000001", "total": 2000}'
        ));

        $response = $this->http->send(new Request('GET', '/leaderboard/absolute/2'));

        self::assertSame(200, $response->getStatusCode());
        self::assertSame(
            '{"00000000-0000-0000-0000-000000000001":2000,"00000000-0000-0000-0000-000000000000":1000}',
            (string) $response->getBody()
        );
    }
}

<?php

declare(strict_types=1);

use Slim\App;
use SP\Ranking\Infrastructure\DI\Services;
use UMA\DIC\Container;

require_once __DIR__ . '/../vendor/autoload.php';

$container = new Container();
$container->register(new Services());

/** @var App $app */
$app = $container->get(App::class);
$app->run();

<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure;

use SP\Ranking\Application\RankingService;
use UMA\Uuid\Uuid;

final class RedisRanking implements RankingService
{
    private \Redis $client;
    private string $sortedSetKey;

    public function __construct(\Redis $client, string $sortedSetKey)
    {
        $this->client       = $client;
        $this->sortedSetKey = $sortedSetKey;
    }

    /**
     * @throws \RedisException If connection is lost sometime after instantiating $this->client
     */
    public function setScore(Uuid $playerId, int $newScore): void
    {
        $this->client->zAdd($this->sortedSetKey, $newScore, (string) $playerId);
    }

    /**
     * @throws \RedisException If connection is lost sometime after instantiating $this->client
     */
    public function alterScore(Uuid $playerId, int $amount): void
    {
        $this->client->zIncrBy($this->sortedSetKey, $amount, (string) $playerId);
    }

    /**
     * @throws \RedisException If connection is lost sometime after instantiating $this->client
     */
    public function absoluteLeaderboard(int $topN): array
    {
        /** @var array<string, float> $leaderboard */
        $leaderboard = $this->client->zRevRange(
            $this->sortedSetKey,
            0,
            max($topN - 1, 0),
            true
        );

        return array_map(intval(...), $leaderboard);
    }

    /**
     * @throws \RedisException If connection is lost sometime after instantiating $this->client
     */
    public function relativeLeaderboard(int $rank, int $range): array
    {
        /** @var array<string, float> $leaderboard */
        $leaderboard = $this->client->zRevRange(
            $this->sortedSetKey,
            max($rank - 1 - $range, 0),
            $rank - 1 + $range,
            true
        );

        return array_map(intval(...), $leaderboard);
    }
}

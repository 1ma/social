<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure\DI;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use SP\Ranking\Application\GetAbsoluteRanking;
use SP\Ranking\Application\GetRelativeRanking;
use SP\Ranking\Application\RankingService;
use SP\Ranking\Application\SubmitPlayerScore;
use SP\Ranking\Infrastructure\Http\Controller\AbsoluteLeaderboard;
use SP\Ranking\Infrastructure\Http\Controller\RelativeLeaderboard;
use SP\Ranking\Infrastructure\Http\Controller\Score;
use SP\Ranking\Infrastructure\Http\Middleware\ContentTypeMiddleware;
use SP\Ranking\Infrastructure\Http\Middleware\JsonSchemaMiddleware;
use SP\Ranking\Infrastructure\RedisRanking;
use UMA\DIC\Container;
use UMA\DIC\ServiceProvider;

final class Services implements ServiceProvider
{
    public function provide(Container $c): void
    {
        $c->set(\Redis::class, static function (): \Redis {
            /** @var string $host */
            $host = $_SERVER['REDIS_HOST'];
            $port = (int) $_SERVER['REDIS_PORT'];

            $client = new \Redis();
            $client->connect($host, $port);

            return $client;
        });

        $c->set(RankingService::class, static function (ContainerInterface $c): RankingService {
            /** @var string $sortedSetKey */
            $sortedSetKey = $_SERVER['REDIS_SET_KEY'];

            /** @var \Redis $redis */
            $redis = $c->get(\Redis::class);

            return new RedisRanking($redis, $sortedSetKey);
        });

        $c->set(SubmitPlayerScore::class, static function (ContainerInterface $c): SubmitPlayerScore {
            /** @var RankingService $ranking */
            $ranking = $c->get(RankingService::class);

            return new SubmitPlayerScore($ranking);
        });

        $c->set(GetAbsoluteRanking::class, static function (ContainerInterface $c): GetAbsoluteRanking {
            /** @var RankingService $ranking */
            $ranking = $c->get(RankingService::class);

            return new GetAbsoluteRanking($ranking);
        });

        $c->set(GetRelativeRanking::class, static function (ContainerInterface $c): GetRelativeRanking {
            /** @var RankingService $ranking */
            $ranking = $c->get(RankingService::class);

            return new GetRelativeRanking($ranking);
        });

        $c->set(Score::class, static function (ContainerInterface $c): Score {
            /** @var SubmitPlayerScore $useCase */
            $useCase = $c->get(SubmitPlayerScore::class);

            return new Score($useCase);
        });

        $c->set(AbsoluteLeaderboard::class, static function (ContainerInterface $c): AbsoluteLeaderboard {
            /** @var GetAbsoluteRanking $useCase */
            $useCase = $c->get(GetAbsoluteRanking::class);

            return new AbsoluteLeaderboard($useCase);
        });

        $c->set(RelativeLeaderboard::class, static function (ContainerInterface $c): RelativeLeaderboard {
            /** @var GetRelativeRanking $useCase */
            $useCase = $c->get(GetRelativeRanking::class);

            return new RelativeLeaderboard($useCase);
        });

        $c->set(ContentTypeMiddleware::class, static function (): MiddlewareInterface {
            return new ContentTypeMiddleware('application/json');
        });

        $c->set(JsonSchemaMiddleware::class, static function (): MiddlewareInterface {
            return new JsonSchemaMiddleware(__DIR__.'/../Contracts/ScoreController.json');
        });

        $c->set(App::class, static function (ContainerInterface $c): App {
            /** @var ContentTypeMiddleware $ctm */
            $ctm = $c->get(ContentTypeMiddleware::class);
            /** @var JsonSchemaMiddleware $jsm */
            $jsm = $c->get(JsonSchemaMiddleware::class);

            $app = AppFactory::create(null, $c);
            $app->post('/score', Score::class)
                ->add($jsm)
                ->add($ctm);
            $app->get('/leaderboard/absolute/{top}', AbsoluteLeaderboard::class);
            $app->get('/leaderboard/relative/{rank}/{range}', RelativeLeaderboard::class);

            $app->addBodyParsingMiddleware();
            $app->addErrorMiddleware(
                (bool) $_SERVER['SLIM_DISPLAY_ERROR_DETAILS'],
                (bool) $_SERVER['SLIM_LOG_ERRORS'],
                (bool) $_SERVER['SLIM_LOG_ERROR_DETAILS'],
            );

            return $app;
        });
    }
}

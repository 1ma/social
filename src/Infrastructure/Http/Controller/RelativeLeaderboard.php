<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure\Http\Controller;

use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use SP\Ranking\Application\GetRelativeRanking;

final class RelativeLeaderboard
{
    private GetRelativeRanking $useCase;

    public function __construct(GetRelativeRanking $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * @param array<string, string> $args
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $rank  = (int) $args['rank'];
        $range = (int) $args['range'];

        $output = $this->useCase->execute(new GetRelativeRanking\Input($rank, $range));

        $body = Stream::create(json_encode($output, \JSON_THROW_ON_ERROR));

        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->withHeader('Content-Length', (string) $body->getSize())
            ->withBody($body);
    }
}

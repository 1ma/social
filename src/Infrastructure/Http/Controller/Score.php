<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure\Http\Controller;

use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use SP\Ranking\Application\SubmitPlayerScore;
use SP\Ranking\Infrastructure\Http\Middleware\JsonSchemaMiddleware;

final class Score
{
    private SubmitPlayerScore $useCase;

    public function __construct(SubmitPlayerScore $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * @param array<string, string> $args
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        /** @var \stdClass $payload */
        $payload = $request->getAttribute(JsonSchemaMiddleware::VALIDATED_PAYLOAD);

        if (isset($payload->total)) {
            $this->useCase->execute(
                new SubmitPlayerScore\Input($payload->user, $payload->total, true)
            );
        } else {
            $this->useCase->execute(
                new SubmitPlayerScore\Input($payload->user, (int) $payload->score, false)
            );
        }

        return new Response(200);
    }
}

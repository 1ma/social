<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure\Http\Middleware;

use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class ContentTypeMiddleware implements MiddlewareInterface
{
    private string $expectedContentType;

    public function __construct(string $expectedContentType)
    {
        $this->expectedContentType = $expectedContentType;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!$request->hasHeader('Content-Type') || $this->expectedContentType !== $request->getHeaderLine('Content-Type')) {
            return new Response(400, [], 'Invalid Content-Type. Expected '.$this->expectedContentType);
        }

        return $handler->handle($request);
    }
}

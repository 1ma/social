<?php

declare(strict_types=1);

namespace SP\Ranking\Infrastructure\Http\Middleware;

use Nyholm\Psr7\Response;
use Opis\JsonSchema\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class JsonSchemaMiddleware implements MiddlewareInterface
{
    public const VALIDATED_PAYLOAD = 'JsonSchemaMiddleware.validated_payload';

    private \stdClass $schema;

    public function __construct(string $schemaPath)
    {
        $this->schema = json_decode(file_get_contents($schemaPath), flags: \JSON_THROW_ON_ERROR);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $payload = (object) $request->getParsedBody();

        $validator = new Validator();
        $result    = $validator->validate($payload, $this->schema);
        if ($result->hasError()) {
            return new Response(400, [], 'Invalid request');
        }

        return $handler->handle($request->withAttribute(self::VALIDATED_PAYLOAD, $payload));
    }
}

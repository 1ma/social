<?php

declare(strict_types=1);

namespace SP\Ranking\Application;

final class SubmitPlayerScore
{
    private RankingService $ranking;

    public function __construct(RankingService $ranking)
    {
        $this->ranking = $ranking;
    }

    public function execute(SubmitPlayerScore\Input $input): void
    {
        if ($input->absolute) {
            $this->ranking->setScore($input->playerId, $input->score);
        } else {
            $this->ranking->alterScore($input->playerId, $input->score);
        }
    }
}

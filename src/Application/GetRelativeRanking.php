<?php

declare(strict_types=1);

namespace SP\Ranking\Application;

final class GetRelativeRanking
{
    private RankingService $ranking;

    public function __construct(RankingService $ranking)
    {
        $this->ranking = $ranking;
    }

    public function execute(GetRelativeRanking\Input $input): GetRelativeRanking\Output
    {
        return new GetRelativeRanking\Output(
            $this->ranking->relativeLeaderboard($input->rank, $input->range)
        );
    }
}

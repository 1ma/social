<?php

declare(strict_types=1);

namespace SP\Ranking\Application;

final class GetAbsoluteRanking
{
    private RankingService $ranking;

    public function __construct(RankingService $ranking)
    {
        $this->ranking = $ranking;
    }

    public function execute(GetAbsoluteRanking\Input $input): GetAbsoluteRanking\Output
    {
        return new GetAbsoluteRanking\Output(
            $this->ranking->absoluteLeaderboard($input->top)
        );
    }
}

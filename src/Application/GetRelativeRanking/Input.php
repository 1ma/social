<?php

declare(strict_types=1);

namespace SP\Ranking\Application\GetRelativeRanking;

final class Input
{
    public int $rank;
    public int $range;

    /**
     * @throws \InvalidArgumentException if $rank or $range are negative
     */
    public function __construct(int $rank, int $range)
    {
        if ($rank < 0) {
            throw new \InvalidArgumentException('Rank must be greater than 0. Got: '.$rank);
        }

        if ($range < 0) {
            throw new \InvalidArgumentException('Range must be greater than 0. Got: '.$range);
        }

        $this->rank  = $rank;
        $this->range = $range;
    }
}

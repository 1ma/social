<?php

declare(strict_types=1);

namespace SP\Ranking\Application\GetAbsoluteRanking;

final class Output implements \JsonSerializable
{
    /** @var array<string, int> */
    private array $ranking;

    /**
     * @param array<string, int> $ranking
     */
    public function __construct(array $ranking)
    {
        $this->ranking = $ranking;
    }

    public function jsonSerialize(): array
    {
        return $this->ranking;
    }
}

<?php

declare(strict_types=1);

namespace SP\Ranking\Application\GetAbsoluteRanking;

final class Input
{
    public int $top;

    /**
     * @throws \InvalidArgumentException if $top is negative
     */
    public function __construct(int $top)
    {
        if ($top < 0) {
            throw new \InvalidArgumentException('Top must be greater than 0. Got: '.$top);
        }

        $this->top = $top;
    }
}

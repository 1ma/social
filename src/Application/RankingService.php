<?php

declare(strict_types=1);

namespace SP\Ranking\Application;

use UMA\Uuid\Uuid;

interface RankingService
{
    public function setScore(Uuid $playerId, int $newScore): void;

    public function alterScore(Uuid $playerId, int $amount): void;

    /**
     * @return array<string, int>
     */
    public function absoluteLeaderboard(int $topN): array;

    /**
     * @return array<string, int>
     */
    public function relativeLeaderboard(int $rank, int $range): array;
}

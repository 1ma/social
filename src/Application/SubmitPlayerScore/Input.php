<?php

declare(strict_types=1);

namespace SP\Ranking\Application\SubmitPlayerScore;

use UMA\Uuid\Uuid;

final class Input
{
    public Uuid $playerId;
    public int $score;
    public bool $absolute;

    /**
     * @throws \InvalidArgumentException if $playerId is not a valid UUID
     */
    public function __construct(string $playerId, int $score, bool $absolute)
    {
        $this->playerId = Uuid::fromString($playerId);
        $this->score    = $score;
        $this->absolute = $absolute;
    }
}

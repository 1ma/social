# Social Point Backend Engineer Take Home Test A: Real Time User Ranking

[![CI](https://github.com/sp-marcel-hernandez/take-home-test/actions/workflows/ci.yml/badge.svg)](https://github.com/sp-marcel-hernandez/take-home-test/actions/workflows/ci.yml)
[![coverage report](https://gitlab.com/1ma/social/badges/master/coverage.svg)](https://gitlab.com/1ma/social/-/commits/master)

Solution submitted by Marcel Hernandez <e6990620@gmail.com>

Find the repo here: https://gitlab.com/1ma/social

## Local Requirements

* PHP 7.4 or 8.0
* Composer
* Docker CE
* docker-compose
* local ports 80 and 6379 free (if they are not, edit docker-compose.yml to use another)

## Setup

```bash
composer setup              // Install dependencies and bring up Dockerized stack
composer static-analysis    // Run deptrac and phpstan
composer test               // Run phpunit tests and phpmetrics
```

## API

```
POST /score     (json endpoint, see schema below)
GET /leaderboard/absolute/{top}
GET /leaderboard/relative/{rank}/{range}
```

Sample Usage:

```
curl -i -H "Content-Type: application/json" -d'{"user":"00000000-0000-0000-0000-000000000000","score":"+100"}' -XPOST localhost/score
curl -i -H "Content-Type: application/json" -d'{"user":"00000000-0000-0000-0000-000000000001","total":-100}' -XPOST localhost/score

curl -i localhost/leaderboard/absolute/10
HTTP/1.1 200 OK
Server: nginx/1.19.6
Date: Sun, 10 Jan 2021 17:25:12 GMT
Content-Type: application/json
Content-Length: 88
Connection: keep-alive
X-Powered-By: PHP/7.4.13

{"00000000-0000-0000-0000-000000000000":100,"00000000-0000-0000-0000-000000000001":-100}


curl -i localhost/leaderboard/relative/1/1
HTTP/1.1 200 OK
Server: nginx/1.19.6
Date: Sun, 10 Jan 2021 17:25:32 GMT
Content-Type: application/json
Content-Length: 88
Connection: keep-alive
X-Powered-By: PHP/7.4.13

{"00000000-0000-0000-0000-000000000000":100,"00000000-0000-0000-0000-000000000001":-100}
```

## Architecture

Whenever I develop a sizable greenfield PHP project I try to stick to the Domain Driven Development style of architecture,
which separates the project in Domain, Application and Infrastructure.

I like to start with the Application layer, because often the requirements can be mapped one to one to "use case" classes
with well-defined inputs and outputs. Designing the core of the application this way eases testing and allows to later
access these use cases from a wide array of entrypoints (http, cli etc.) with no coupling.

After iterating the use cases a bit I extract common logic and entities from it and move them to the Domain layer.
In this instance, the project is so simple that I struggled to find any and omitted this layer altogether.

Finally, the Infrastructure layer is where all the code coupled to external elements of the application goes, such as
HTTP controllers and middlewares.
I'm afraid I willfully ignored one of the requirements of the exercise (not using any external storage system), because
Redis sorted sets fitted the problem of building a leaderboard like a glove. Nevertheless, there's an in-memory fake
implementation of the leaderboard in the unit tests directory.

## Testing

There are three types of tests: unit, integration and end to end (a.k.a. functional).

Unit tests are tests that can run in isolation, without relying on any external piece of infrastructure.

Integration tests are test that need some kind of external service to run, in this case a Redis service.

Finally, end to end tests are tests that exercise the whole application directly though the HTTP API (note that
the reported coverage % is artificially low because PHPUnit cannot report coverage on these tests).

## QA and CI/CD and OSS

I also demoed how to enforce a clean architecture with [deptrac](https://github.com/sensiolabs-de/deptrac) and the
code (mostly) typed with [phpstan](https://github.com/phpstan/phpstan) and [phpmetrics](https://www.phpmetrics.org/).

Besides the conveniece scripts in the composer.json file for the developer I've also set up a [GitLab pipeline](https://gitlab.com/1ma/social/-/pipelines)
to run all these checks and tests on every push to master.

Finally, I built the solution with a couple of [my own PHP open source libraries](https://packagist.org/packages/uma/) that I felt could be useful:

* [uma/uuid](https://github.com/1ma/uuid): A SOLID-er UUID implementation than the widely used [ramsey/uuid](https://github.com/ramsey/uuid).
* [uma/dic](https://github.com/1ma/dic): A type-safe and very minimalistic Dependency Injection Container.
